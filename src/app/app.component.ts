import { Component, OnInit } from '@angular/core';
import { modules } from './@core/constante/app.constante';
const delay =(s:number)=>{
  return new Promise((res,rej)=>{
    setTimeout(()=>{
      res(true)
    },s)
  })
}
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [...modules],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'aya-gasy-ui';
  test=true;
  

  ngOnInit(): void {
    
  }
  
}
