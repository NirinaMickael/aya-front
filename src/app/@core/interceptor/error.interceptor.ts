import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject, Inject } from '@angular/core';
import { catchError, throwError } from 'rxjs';
import { HelpersService } from '../services/helper.service';
import { MatSnackBar } from '@angular/material/snack-bar';
export const ErrorInterceptor: HttpInterceptorFn = (req, next) => {
  const snackBar = inject(MatSnackBar);
  const helpersService = new HelpersService(snackBar);
  return next(req).pipe(
    catchError((err: any) => {
      helpersService.notify(err.message) 
      if (err instanceof HttpErrorResponse) {
        // if (err.status === 401) {     
        //   console.error('Unauthorized request:', err);
        // } else {
        //  helpersService.notify(err.message)
        //   console.error('HTTP error:', err);
        // }
        helpersService.notify(err.message) 
      } else {
        console.error('An error occurred:', err);
      }
      return throwError(() => err); 
    })
  );;
};