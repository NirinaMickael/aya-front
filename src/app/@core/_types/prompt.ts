export interface IPrompt {
    id: number
    Text: string
    CreatedCount: number
    UpdatedCount: number
  }
  

  export interface IRank {
    id: string
    ranking :number,
    Nom: string
    Prenom: any
    Email: string
    Picture: string
    promptCount: number
    score: number
    grade: string
  }
  