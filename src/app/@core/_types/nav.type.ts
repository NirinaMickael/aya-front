export type NavRoute = {
    label: string;
    path: string
}
export interface MenuItem {
    title: string;
    active?: boolean;
    path?: string;
    subItems?: SubMenuItem[];
}

export interface SubMenuItem {
    title: string;
    path?: string
}
