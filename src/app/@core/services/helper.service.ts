import { Injectable } from '@angular/core';

import {
    MatSnackBar,
  } from '@angular/material/snack-bar';
@Injectable()
export class HelpersService {

  constructor(public snackBar: MatSnackBar) {}

  notify(message: string, action: string="error", duration: number=50000) {
    console.log("message",message);
    
    return this.snackBar.open(message, action, {duration});
  }
}