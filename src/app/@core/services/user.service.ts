
import { Injectable } from "@angular/core";
import { MainService } from "./main.service";
import { IUser } from "../_types/user.type";
@Injectable()
export class UserService{
    constructor(private mainService:MainService){}
    createUser(payload:IUser){
        console.log("created");
        return this.mainService.POST<IUser>("user/create",payload);
    }
    getUserById(userId:string){
        return this.mainService.GET(`user/${userId}`);
    }
}