import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable()
export class MainService{
    SERVER_URL=environment.API

    constructor(private httpClient:HttpClient){}

    GET(api:string){
        return this.httpClient.get(`${this.SERVER_URL}${api}`);
    }
    POST<T>(api:string,payload:T){
        console.log(`${this.SERVER_URL}${api}`);
        
        return this.httpClient.post(`${this.SERVER_URL}${api}`,payload)
    }
}