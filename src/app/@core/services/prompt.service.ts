import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MainService } from "./main.service";
import { BehaviorSubject, forkJoin, Observable } from "rxjs";
@Injectable()
export class PromptService{
    refreshCreated$ = new  BehaviorSubject<boolean>(false)
    constructor(private mainService:MainService){}
    getPrompt(type:string,cursor:number){
        return this.mainService.GET(`prompt?type=${type}`);
    }
    createPrompt(payload:any[]){
        return this.mainService.POST<any[]>(`prompt-on-sentiment`,payload)
    }
    getAllSentiment(){
        return this.mainService.GET(`sentiment`);
    }
    getContribution(userId:string,type:string){
        return this.mainService.GET(`prompt-on-sentiment?type=${type}&id=${userId}`)
    }

    getLeaderBoard(page:number,pageSize:number){
        return this.mainService.GET(`prompt-on-sentiment/leader?page=${page}&pageSize=${pageSize}`)
    }
}