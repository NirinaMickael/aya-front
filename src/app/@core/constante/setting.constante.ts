import { RouterModule } from "@angular/router";
import { SidebarComponent } from "../../pages/settings/components/sidebar/sidebar.component";
import { MenuItem, SubMenuItem } from "../_types/nav.type";

export const settingModule = [
    RouterModule,
    SidebarComponent
]

// Dans votre fichier de composant
export const menuItems: MenuItem[] = [

    {
        title: 'Accounts',
        active: true,
        path: 'account-setting'
    },
    {
        title: 'Users',
        active: false,
        path: 'user-setting'
    },
    {
        title: 'Appareance',
        active: false,
        path: 'appareance-setting'
    },

];

export const subMenuItems: SubMenuItem[] = [
    { title: 'Accounts' },
    { title: 'Team' },
    { title: 'Others' },
];
