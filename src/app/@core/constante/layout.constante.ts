import { RouterOutlet } from "@angular/router";
import { ContributionsComponent } from "../../pages/contributions/contributions.component";
import { SettingsComponent } from "../../pages/settings/settings.component";
import { WorkspaceComponent } from "../../pages/workspace/workspace.component";
import { NavComponent } from "../../shared/features/nav/nav.component";

export const LayoutModules = [
    NavComponent,
    WorkspaceComponent,
    SettingsComponent,
    ContributionsComponent,
    RouterOutlet
]