import { RouterLink, RouterOutlet } from "@angular/router";
import { AuthComponent } from "../../pages/auth/auth.component";
import { CommonModule } from "@angular/common";
import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { WorkspaceComponent } from "../../pages/workspace/workspace.component";
import { ContributionsComponent } from "../../pages/contributions/contributions.component";
import { SettingsComponent } from "../../pages/settings/settings.component";

export const modules = [
    RouterLink,
    AuthComponent,
    DashboardComponent,
    WorkspaceComponent,
    ContributionsComponent,
    SettingsComponent,
    RouterLink,
    CommonModule,
    RouterOutlet
]