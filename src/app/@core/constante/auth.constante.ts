import { RouterLink, RouterOutlet } from "@angular/router";
import { LoginComponent } from "../../shared/features/login/login.component";
import { SignupComponent } from "../../shared/features/signup/signup.component";

export const auhtModules = [
    LoginComponent,
    SignupComponent,
    RouterLink,
    RouterOutlet
]