import { Routes } from '@angular/router';
import { authRoutes } from './pages/auth/auth.routes';
import { layoutRoutes } from './pages/layout/layout.routes';
import { settingsRoute } from './pages/settings/setting.routes';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { AuthGuard } from '@auth0/auth0-angular';

export const routes: Routes = [
    {
        path: '',
        component:LandingPageComponent,
    },
    {
        path: 'auth',
        title: "Authentification",
        loadComponent: () => import("./pages/auth/auth.component").then((c) => c.AuthComponent),
        children: [
            ...authRoutes
        ]
    },
    {
        path: 'layout',
        title: "",
        loadComponent: () => import("./pages/layout/layout.component").then((c) => c.LayoutComponent),
        children: [
            ...layoutRoutes
        ],
        canActivate:[AuthGuard]
    },
    {
        path: 'settings',
        title: 'Settings',
        loadComponent: () => import('./pages/settings/settings.component').then(c => c.SettingsComponent),
        children: [
            ...settingsRoute
        ],
        canActivate:[AuthGuard]
    }
];
