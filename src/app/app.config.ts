import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideAuth0 } from '@auth0/auth0-angular';
import { environment } from '../environments/environment';
import { PromptService } from './@core/services/prompt.service';
import { MainService } from './@core/services/main.service';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { HelpersService } from './@core/services/helper.service';
import { ErrorInterceptor } from './@core/interceptor/error.interceptor';
import { UserService } from './@core/services/user.service';




export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), provideAnimationsAsync(),
    PromptService,MainService,
    HelpersService,
    UserService,
    provideHttpClient(withInterceptors([ErrorInterceptor])),
    provideAuth0({
      domain: environment.DOMAIN,
      clientId: environment.CLIENTID,
      authorizationParams: {
        redirect_uri: window.location.origin
      }
    }),
  ]
};
