import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { workspaceModules } from '../../@core/constante/work.constante';
import { PromptService } from '../../@core/services/prompt.service';
import { IPrompt } from '../../@core/_types/prompt';
import { ISentiment } from '../../@core/_types/sentiment';
import { forkJoin, Subscription } from 'rxjs';
import { AuthService, User } from '@auth0/auth0-angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-workspace',
  standalone: true,
  imports: [...workspaceModules],
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent {
  prompt!: IPrompt;
  sentiments!: ISentiment[];
  user!: User | null | undefined;
  subscription!: Subscription;
  cursor: number = 0;
  type : string = "CreatedCount"
  constructor(private promptService: PromptService,
    private auth: AuthService,
    private router: Router,
    private activeRouter: ActivatedRoute
  ) {
    // this.activeRouter.queryParamMap
    //   .subscribe(({params}:any) => {
    //     console.log("params", params);
    //     this.cursor = +(params?.skip || "0");        
    //     this.type = (params?.type || "CreatedCount");
    //     this.loadData(this.cursor,this.type);
    //   });
      this.loadData(this.cursor,this.type)
    // this.activeRouter.params.subscribe((data: any) => {
    //   this.cursor = +(data?.skip || "0")
    //   this.loadData(this.cursor);
    // })
    this.auth.user$.subscribe((user) => {
      if (user) {
        console.log("user",user);
        
        this.user = user;
      }
    });

    // this.promptService.refreshCreated$.subscribe((data) => {
    //   if (data) {
    //     this.cursor += 1;
    //     this.router.navigateByUrl(`/layout/workspace/${this.cursor}`);
    //   }
    // });

  }
  // ngOnInit(): void {
  //   this.loadData(this.cursor);
  // }
  loadData(cursor: number,type:string) {
    this.subscription = forkJoin({
      allPrompts: this.promptService.getPrompt(type, cursor),
      sentiments: this.promptService.getAllSentiment(),
    }).subscribe({
      next: (data) => {
        this.prompt = ((data.allPrompts as any).prompts);
        this.cursor = ((data.allPrompts as any).cursor);
        this.sentiments = data.sentiments as ISentiment[];
        console.log("this",data);
        
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  onSubmitSuccess(): void {
    // Recharger les données après une soumission réussie
    this.loadData(this.cursor, this.type);
  }

}
