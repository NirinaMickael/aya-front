import { Component, OnDestroy, OnInit } from '@angular/core';
import { RankComponent } from '../../shared/components/rank/rank.component';
import { ListRankComponent } from '../../shared/components/list-rank/list-rank.component';
import { Subscription } from 'rxjs';
import { PromptService } from '../../@core/services/prompt.service';
import { IRank } from '../../@core/_types/prompt';

@Component({
  selector: 'app-leaderboard',
  standalone: true,
  imports: [RankComponent,ListRankComponent],
  templateUrl: './leaderboard.component.html',
  styleUrl: './leaderboard.component.scss'
})
export class LeaderboardComponent implements OnInit,OnDestroy {
  private subscriptions : Subscription = new Subscription();
  ranks : IRank[]=[];
  constructor( private promptService :PromptService){}
  ngOnInit(): void {
    const leaderSubscription = this.promptService.getLeaderBoard(0,10).subscribe({
      next:(value:any)=>{
        this.ranks = value.data as IRank[];
      },
      error :(error)=>{
        console.log("Error",error);
        
      }
    });
    this.subscriptions.add(leaderSubscription);
  }
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
