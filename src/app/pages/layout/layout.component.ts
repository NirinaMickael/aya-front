import { Component } from '@angular/core';
import { LayoutModules } from '../../@core/constante/layout.constante';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-layout',
  standalone: true,
  imports: [...LayoutModules],
  templateUrl: './layout.component.html',
  styleUrl: './layout.component.scss'
})
export class LayoutComponent {
 
}
