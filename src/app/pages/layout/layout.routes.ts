import { Routes } from "@angular/router";

export const layoutRoutes : Routes = [
    {
        path:'',
        redirectTo:'workspace',
        pathMatch:'full'
    },
    {
        path:'contributions',
        title:'Contributions',
        loadComponent:()=>import('../contributions/contributions.component').then(c=>c.ContributionsComponent)
    },
    {
        path:'contributions/:type',
        title:'Contributions',
        loadComponent:()=>import('../contributions/contributions.component').then(c=>c.ContributionsComponent)
    },
    {
        path:'workspace',
        title:'Workspace',
        loadComponent:()=>import('../workspace/workspace.component').then(c=>c.WorkspaceComponent)
    },
    // {
    //     path:'workspace/:skip',
    //     title:'Workspace',
    //     loadComponent:()=>import('../workspace/workspace.component').then(c=>c.WorkspaceComponent)
    // },
    {
        path:'leaderboard',
        title:'Leaderboard',
        loadComponent:()=>import('../leaderboard/leaderboard.component').then(c=>c.LeaderboardComponent)
    }
]