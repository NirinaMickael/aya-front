import { Component } from '@angular/core';
import { auhtModules } from '../../@core/constante/auth.constante';
import { authRoutes } from './auth.routes';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [...auhtModules],
  providers: [],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss'
})
export class AuthComponent {

}
