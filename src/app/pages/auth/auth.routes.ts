import { Routes } from "@angular/router";
export const authRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch:"full"
    },
    {
        path: "login",
        title: "Login",
        loadComponent:()=>import("../../shared/features/login/login.component").then((c)=>c.LoginComponent)
    },
    {
        path: "signup",
        title: "Signup",
        loadComponent:()=>import("../../shared/features/signup/signup.component").then((c)=>c.SignupComponent)
    }
]