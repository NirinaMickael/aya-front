import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { SidebarContributionComponent } from './sidebar-contribution/sidebar-contribution.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PromptService } from '../../@core/services/prompt.service';
import { UserService } from '../../@core/services/user.service';
import { AuthService } from '@auth0/auth0-angular';
import { Subject, tap, switchMap, catchError, of, takeUntil } from 'rxjs';
import { HelpersService } from '../../@core/services/helper.service';
import { IPrompt } from '../../@core/_types/prompt';
import { ISentiment } from '../../@core/_types/sentiment';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-contributions',
  standalone: true,
  imports: [MatTableModule, MatPaginatorModule,SidebarContributionComponent,
    MatSelectModule,
    MatCheckboxModule,
    CommonModule
  ],
  templateUrl: './contributions.component.html',
  styleUrl: './contributions.component.scss'
})
export class ContributionsComponent {
  labels = [
    {
      name:'faly',
      selected:false,
      id:1
    },
    {
      name:'tezitra',
      selected:false,
      id:2
    },
    {
      name:'mitaintaina',
      selected:false,
      id:3
    },
    {
      name:'mankahala',
      selected:false,
      id:4
    },
    {
      name:'malahelo',
      selected:false,
      id:5
    },
    {
      name:'Tia',
      selected:false,
      id:6
    },
    {
      name:'leo be',
      selected:false,
      id:7
    },
    {
      name:'miomehy',
      selected:false,
      id:8
    },
    {
      name:'gaga',
      selected:false,
      id:9
    },
    {
      name:'diso fanantenana',
      selected:false,
      id:10
    }
  ]
  private destroy$ = new Subject<void>();
  public contributions: any = {};
  public type: string='CREATE';
  public currentPrompt :any;
  sentiments: ISentiment[]=[];
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private promptOnService: PromptService,
    private helperService : HelpersService,
    private  activeRouter : ActivatedRoute,
    private router : Router
  ) {
    
  }

  ngOnInit() {
    this.activeRouter.params.subscribe((data:any)=>{
      this.type = data?.type || "CREATE"       
      this.loadData(); 
    })
  }
  loadData(){
    console.log("UPDATE",this.type);
    
    this.promptOnService.getAllSentiment().subscribe((sentiments)=>{
      console.log("this.sentiments",this.sentiments);
      
      this.sentiments = sentiments as ISentiment[]
    })
    this.authService.user$.pipe(
      tap((authData) => {
        if (!authData?.sub) {
          throw new Error('No authData.sub found');
        }
      }),
      switchMap((authData) => this.userService.getUserById(authData?.sub as string)),
      tap((userData: any) => {
        if (!userData?.data) {
          this.helperService.notify('No userData.id found')
          throw new Error('No userData.id found'); 
        }
      }),
      switchMap((userData: any) => this.promptOnService.getContribution(userData.data.id, this.type)),
      takeUntil(this.destroy$)
    ).subscribe(
      (contributionData: any) => {
        this.contributions = contributionData;
        const id = (this.contributions.data.prompts as any[]).at(-1)?.id || -1 
        this.getPromptAndSentimentsById(id);
        console.log("contributionData",contributionData);
        
      }
    );
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
  FilterByType(type:string){
    this.type = type;
    this.router.navigateByUrl(`/layout/contributions/${this.type}`);
  }
  getPromptAndSentimentsById(id:number) {
    const data = this.contributions.data;
    const index = data.prompts.findIndex((prompt:any) => prompt.id === id);
    if (index === -1) {
      this.currentPrompt = null;
      return null;
    }
    data.sentiments[index]
    this.currentPrompt = {
      prompt: data.prompts[index],
      sentiments: this.sentiments.map((sentiment:ISentiment)=>{
        return {
          ...sentiment,
          isSelected : data.sentiments[index].includes(sentiment.id) ? true:false
        }
      })
    }
    return null;
  }
}