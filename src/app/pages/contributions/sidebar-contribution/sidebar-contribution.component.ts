import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IPrompt } from '../../../@core/_types/prompt';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-sidebar-contribution',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sidebar-contribution.component.html',
  styleUrl: './sidebar-contribution.component.scss'
})
export class SidebarContributionComponent implements OnInit {
  @Input() prompts!: IPrompt[];
  current : number =0;
  @Output() onSetCurrent = new EventEmitter<number>();
  constructor() {
    console.log("this.prompts",this.prompts);
  }
  ngOnInit(): void {
    console.log("this.prompts",this.prompts);
    
  }
  setCurrent(id:number){
    this.current = id;
    this.onSetCurrent.emit(id);
  }
}