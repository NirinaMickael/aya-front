import { Component, OnDestroy } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AuthService, User } from '@auth0/auth0-angular';
import { IUser } from '../../@core/_types/user.type';
import { UserService } from '../../@core/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-landing-page',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'] // Fixed typo
})
export class LandingPageComponent implements OnDestroy {
  private subscriptions: Subscription = new Subscription();

  constructor(
    private auth: AuthService,
    private router: Router,
    private userService: UserService
  ) {
    this.initAuthSubscriptions();
  }

  private initAuthSubscriptions(): void {
    const isAuthenticatedSubscription = this.auth.isAuthenticated$.subscribe({
      next: (isAuthenticated) => this.handleAuthentication(isAuthenticated),
      error: (error) => console.error('Authentication error', error),
    });

    const userSubscription = this.auth.user$.subscribe({
      next: (user) => this.handleUser(user),
      error: (error) => console.error('User data error', error),
    });

    this.subscriptions.add(isAuthenticatedSubscription);
    this.subscriptions.add(userSubscription);
  }

  private handleAuthentication(isAuthenticated: boolean): void {
    if (isAuthenticated) {
      console.log('User is authenticated');
      this.router.navigateByUrl('/layout');
    }
  }

  private handleUser(user: User | null | undefined): void {
    if (user) {
      const payload: IUser = {
        id: user.sub as string,
        Email: user.email as string,
        Mot_de_passe: '',
        Nom: user.nickname as string,
        Prenom: user.given_name as string,
        Numero: user.phone_number as string,
        Picture: user.picture as string
      };
      this.userService.createUser(payload).subscribe((data)=>{
        console.log("response",data);
        
      });
    }
  }

  login(): void {
    this.auth.loginWithRedirect({
      appState: { target: '/layout' }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
