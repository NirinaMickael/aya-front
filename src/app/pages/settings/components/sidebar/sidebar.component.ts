import { Component } from '@angular/core';
import { menuItems, subMenuItems } from '../../../../@core/constante/setting.constante';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent {
  subMenuItems_ = subMenuItems
  menuItems_ = menuItems
}
