import { Component } from '@angular/core';
import { settingModule } from '../../@core/constante/setting.constante';

@Component({
  selector: 'app-settings',
  standalone: true,
  imports: [...settingModule],
  templateUrl: './settings.component.html',
  styleUrl: './settings.component.scss'
})
export class SettingsComponent {

}
