import { Route } from "@angular/router";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { AccountSettingComponent } from "./components/account-setting/account-setting.component";

export const settingsRoute: Route[] = [
    {
        path: '',
        title: '',
        pathMatch: 'full',
        redirectTo: 'account-setting'
    },
    {
        path: 'account-setting',
        title: 'Account Settings',
        loadComponent : () => import('./components/account-setting/account-setting.component').then(c=>c.AccountSettingComponent)
    },
    {
        path: 'team-setting',
        title: 'Account Settings',
        loadComponent : () => import('./components/account-setting/account-setting.component').then(c=>c.AccountSettingComponent)
    },
    {
        path: 'user-setting',
        title: 'Account Settings',
        loadComponent : () => import('./components/account-setting/account-setting.component').then(c=>c.AccountSettingComponent)
    },
    {
        path: 'appareance-setting',
        title: 'Appareance Settings',
        loadComponent : () => import('./components/appareance-setting/appareance-setting.component').then(c=>c.AppareanceSettingComponent)
    }
]