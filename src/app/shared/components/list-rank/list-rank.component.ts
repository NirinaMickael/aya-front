import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { IRank } from '../../../@core/_types/prompt';
@Component({
  selector: 'app-list-rank',
  standalone: true,
  imports: [MatTableModule, MatPaginatorModule],
  templateUrl: './list-rank.component.html',
  styleUrl: './list-rank.component.scss'
})
export class ListRankComponent implements OnInit {
  @Input() ranks : IRank[]=[];
  displayedColumns: string[] = ['ranking','Picture', 'username', 'score', 'grade'];
  dataSource = new MatTableDataSource<IRank>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  ngOnInit(): void {
    console.log(this.ranks);
    
    this.dataSource = new MatTableDataSource<IRank>(this.ranks);
  }
}