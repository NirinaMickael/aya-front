import { Component, Input } from '@angular/core';
import { IRank } from '../../../@core/_types/prompt';
import { nameFormat } from '../../../@core/utils/nameFormat';

@Component({
  selector: 'app-rank',
  standalone: true,
  imports: [],
  templateUrl: './rank.component.html',
  styleUrl: './rank.component.scss'
})
export class RankComponent {
  private _userData!: IRank;
  @Input() 
  set userData(value:IRank) {
   this._userData=value;
   this._userData.Nom = nameFormat(this._userData.Nom);
  }
  get userData() : IRank{
    return this._userData;
  }
}
