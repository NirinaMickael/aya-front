import { Component } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { NavRoute } from '../../../@core/_types/nav.type';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { AuthService, User } from '@auth0/auth0-angular';
@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, MatMenuModule, MatIconModule, MatDividerModule],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.scss'
})
export class NavComponent {

  routes: NavRoute[] = [
    {
      label: "Workspace",
      path: 'workspace',
    },
    {
      label: 'Contributions',
      path: "contributions",
    },
    {
      label: 'Settings',
      path: '/settings'
    },
    {
      label: 'Logout',
      path: 'logout'
    }
  ];
  currentUser: User | null | undefined;
  constructor(private router: Router, private auth: AuthService) {
    this.auth.user$.subscribe((data) => {
      this.currentUser = data;
    })
  }
  toLeaderboard() {

    // this.router.navigate(["/leaderboard"])
  }
  logout() {
    this.auth.logout({
      openUrl(url) {
        window.location.replace(url);
      }
    });
  }
}
