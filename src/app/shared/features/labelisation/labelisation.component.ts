import { Component, Input, OnInit, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { PromptService } from '../../../@core/services/prompt.service';
import { IPrompt } from '../../../@core/_types/prompt';
import { ISentiment } from '../../../@core/_types/sentiment';
import { User } from '@auth0/auth0-angular';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';

@Component({
  selector: 'app-labelisation',
  standalone: true,
  imports: [ReactiveFormsModule, MatCheckboxModule, 
    MatButtonModule, MatIconModule,
    MatSelectModule],
  templateUrl: './labelisation.component.html',
  styleUrls: ['./labelisation.component.scss']
})
export class LabelisationComponent implements OnInit, OnChanges {
  labelForms!: FormGroup;
  @Input() prompt!: IPrompt;
  @Input() sentiments!: ISentiment[];
  @Input() cursor : number =0;
  @Input() type : string="CreatedCount";
  @Input() user!: User | null | undefined;
  isLoading: boolean = false;
  isValid : boolean = true;
  @Output() submitSuccess: EventEmitter<void> = new EventEmitter<void>();
  constructor(private formBuilder: FormBuilder, private promptService: PromptService,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['prompt'] || changes['sentiments']) {
      this.initializeForm();
    }
  }

  initializeForm(): void {
    this.labelForms = this.formBuilder.group({
      idPrompt: [this.prompt?.Text, Validators.required],
      idSentiment: this.formBuilder.array(this.sentiments.map(x => !1)),
      type: ['CREATE'],
      Author: [(this.user as User)?.sub]
    });
  }

  onCheckboxChange(event: any, index: number): void {

    const selectedSentiments = this.labelForms.get('idSentiment') as FormArray;
    selectedSentiments.at(index).setValue(event.checked);
    this.isValid = (this.labelForms.get('idSentiment')?.value as any[]).every(e=>!e)
    
  }

  handleSubmit(): void {
    this.isLoading = true;
    let data = this.labelForms.get('idSentiment')?.value;
    data = this.sentiments.filter((sentiment)=>{
      return data[sentiment.id-1]
    })
    const payload = [{
      ...this.labelForms.value,
      idSentiment:data.map((t:any)=>t.id),
      idPrompt: this.prompt.id
    }];  
    this.promptService.createPrompt(payload).subscribe(() => {
      this.isLoading = false;
      this.clearForm();
      this.cursor+=1

      this.submitSuccess.emit();
      this.router.navigate(
        ['/layout/workspace'],
        { queryParams: { skip: this.cursor,type:this.type } }
      );

    });
  }

  onChangeTask(event:any){
    this.router.navigate(
      ['/layout/workspace'],
      { queryParams: { skip: this.cursor ,type:event.value} }
    );
  }

  clearForm(): void {
    this.labelForms.reset({
      idPrompt: this.prompt?.Text,
      idSentiment: [],
      type: 'CREATE',
      Author: (this.user as User)?.sub
    });
  }
  handleSkip(){
    this.clearForm();
    this.submitSuccess.emit();
  }
}
